package ru.mtumanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.client.*;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ProjectEndpointClient getProjectEndpoint();

    @NotNull
    TaskEndpointClient getTaskEndpoint();

    @NotNull
    UserEndpointClient getUserEndpoint();

    @NotNull
    AuthEndpointClient getAuthEndpoint();

    @NotNull
    DomainEndpointClient getDomainEndpoint();

    @NotNull
    SystemEndpointClient getSystemEndpoint();

}
