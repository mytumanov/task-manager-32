package ru.mtumanov.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.api.endpoint.IAuthEndpoint;
import ru.mtumanov.tm.dto.request.user.UserLoginRequest;
import ru.mtumanov.tm.dto.request.user.UserLogoutRequest;
import ru.mtumanov.tm.dto.response.user.UserLoginResponse;
import ru.mtumanov.tm.dto.response.user.UserLogoutResponse;

public class AuthEndpointClient extends AbstractEndpoint implements IAuthEndpoint {

    @Override
    @SneakyThrows
    @NotNull
    public UserLoginResponse login(@NotNull UserLoginRequest request) {
        return call(request, UserLoginResponse.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public UserLogoutResponse logout(@NotNull UserLogoutRequest request) {
        return call(request, UserLogoutResponse.class);
    }

}
