package ru.mtumanov.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.api.endpoint.IDomainEndpoint;
import ru.mtumanov.tm.dto.request.data.*;
import ru.mtumanov.tm.dto.response.data.*;

public class DomainEndpointClient extends AbstractEndpoint implements IDomainEndpoint {

    @Override
    @SneakyThrows
    @NotNull
    public DataBase64LoadResponse loadDataBase64(@NotNull DataBase64LoadRequest request) {
        return call(request, DataBase64LoadResponse.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public DataBase64SaveResponse saveDataBase64(@NotNull DataBase64SaveRequest request) {
        return call(request, DataBase64SaveResponse.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public DataBinaryLoadResponse loadDataBinary(@NotNull DataBinaryLoadRequest request) {
        return call(request, DataBinaryLoadResponse.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public DataBinarySaveResponse saveDataBinary(@NotNull DataBinarySaveRequest request) {
        return call(request, DataBinarySaveResponse.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public DataJsonLoadFasterXmlResponse loadDataJsonFasterXml(@NotNull DataJsonLoadFasterXmlRequest request) {
        return call(request, DataJsonLoadFasterXmlResponse.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public DataJsonSaveFasterXmlResponse saveDataJsonFasterXml(@NotNull DataJsonSaveFasterXmlRequest request) {
        return call(request, DataJsonSaveFasterXmlResponse.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public DataJsonLoadJaxbResponse loadDataJsonJaxb(@NotNull DataJsonLoadJaxbRequest request) {
        return call(request, DataJsonLoadJaxbResponse.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public DataJsonSaveJaxbResponse saveDataJsonJaxb(@NotNull DataJsonSaveJaxbRequest request) {
        return call(request, DataJsonSaveJaxbResponse.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public DataXmlLoadFasterXmlResponse loadDataXmlFasterXml(@NotNull DataXmlLoadFasterXmlRequest request) {
        return call(request, DataXmlLoadFasterXmlResponse.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public DataXmlSaveFasterXmlResponse saveDataXmlFasterXml(@NotNull DataXmlSaveFasterXmlRequest request) {
        return call(request, DataXmlSaveFasterXmlResponse.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public DataXmlLoadJaxbResponse loadDataXmlJaxb(@NotNull DataXmlLoadJaxbRequest request) {
        return call(request, DataXmlLoadJaxbResponse.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public DataXmlSaveJaxbResponse saveDataXmlJaxb(@NotNull DataXmlSaveJaxbRequest request) {
        return call(request, DataXmlSaveJaxbResponse.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public DataYamlLoadFasterXmlResponse loadDataYamlFasterXml(@NotNull DataYamlLoadFasterXmlRequest request) {
        return call(request, DataYamlLoadFasterXmlResponse.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public DataYamlSaveFasterXmlResponse saveDataYamlFasterXml(@NotNull DataYamlSaveFasterXmlRequest request) {
        return call(request, DataYamlSaveFasterXmlResponse.class);
    }

}
