package ru.mtumanov.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.api.endpoint.IProjectEndpoint;
import ru.mtumanov.tm.dto.request.project.*;
import ru.mtumanov.tm.dto.response.project.*;

public class ProjectEndpointClient extends AbstractEndpoint implements IProjectEndpoint {

    @Override
    @SneakyThrows
    @NotNull
    public ProjectChangeStatusByIdResponse projectChangeStatusById(
            @NotNull ProjectChangeStatusByIdRequest request
    ) {
        return call(request, ProjectChangeStatusByIdResponse.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public ProjectChangeStatusByIndexResponse projectChangeStatusByIndex(
            @NotNull ProjectChangeStatusByIndexRequest request
    ) {
        return call(request, ProjectChangeStatusByIndexResponse.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public ProjectClearResponse projectClear(@NotNull ProjectClearRequest request) {
        return call(request, ProjectClearResponse.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public ProjectCompleteByIdResponse projectCompleteById(@NotNull ProjectCompleteByIdRequest request) {
        return call(request, ProjectCompleteByIdResponse.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public ProjectCompleteByIndexResponse projectCompleteByIndex(
            @NotNull ProjectCompleteByIndexRequest request) {
        return call(request, ProjectCompleteByIndexResponse.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public ProjectCreateResponse projectCreate(@NotNull ProjectCreateRequest request) {
        return call(request, ProjectCreateResponse.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public ProjectListResponse projectList(@NotNull ProjectListRequest request) {
        return call(request, ProjectListResponse.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public ProjectRemoveByIdResponse projectRemoveById(@NotNull ProjectRemoveByIdRequest request) {
        return call(request, ProjectRemoveByIdResponse.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public ProjectRemoveByIndexResponse projectRemoveByIndex(@NotNull ProjectRemoveByIndexRequest request) {
        return call(request, ProjectRemoveByIndexResponse.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public ProjectShowByIdResponse projectShowById(@NotNull ProjectShowByIdRequest request) {
        return call(request, ProjectShowByIdResponse.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public ProjectShowByIndexResponse projectShowByIndex(@NotNull ProjectShowByIndexRequest request) {
        return call(request, ProjectShowByIndexResponse.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public ProjectStartByIdResponse projectStartById(@NotNull ProjectStartByIdRequest request) {
        return call(request, ProjectStartByIdResponse.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public ProjectStartByIndexResponse projectStartByIndex(@NotNull ProjectStartByIndexRequest request) {
        return call(request, ProjectStartByIndexResponse.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public ProjectUpdateByIdResponse projectUpdateById(@NotNull ProjectUpdateByIdRequest request) {
        return call(request, ProjectUpdateByIdResponse.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public ProjectUpdateByIndexResponse projectUpdateByIndex(@NotNull ProjectUpdateByIndexRequest request) {
        return call(request, ProjectUpdateByIndexResponse.class);
    }

}
