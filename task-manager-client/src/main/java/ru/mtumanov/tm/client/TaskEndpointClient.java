package ru.mtumanov.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.api.endpoint.ITaskEndpoint;
import ru.mtumanov.tm.dto.request.task.*;
import ru.mtumanov.tm.dto.response.task.*;

public class TaskEndpointClient extends AbstractEndpoint implements ITaskEndpoint {

    @Override
    @SneakyThrows
    @NotNull
    public TaskChangeStatusByIdResponse taskChangeStatusById(@NotNull TaskChangeStatusByIdRequest request) {
        return call(request, TaskChangeStatusByIdResponse.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public TaskChangeStatusByIndexResponse taskChangeStatusByIndex(
            @NotNull TaskChangeStatusByIndexRequest request
    ) {
        return call(request, TaskChangeStatusByIndexResponse.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public TaskClearResponse taskClear(@NotNull TaskClearRequest request) {
        return call(request, TaskClearResponse.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public TaskCompleteByIdResponse taskCompleteById(@NotNull TaskCompleteByIdRequest request) {
        return call(request, TaskCompleteByIdResponse.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public TaskCompleteByIndexResponse taskCompleteByIndex(@NotNull TaskCompleteByIndexRequest request) {
        return call(request, TaskCompleteByIndexResponse.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public TaskCreateResponse taskCreate(@NotNull TaskCreateRequest request) {
        return call(request, TaskCreateResponse.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public TaskListResponse taskList(@NotNull TaskListRequest request) {
        return call(request, TaskListResponse.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public TaskRemoveByIdResponse taskRemoveById(@NotNull TaskRemoveByIdRequest request) {
        return call(request, TaskRemoveByIdResponse.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public TaskRemoveByIndexResponse taskRemoveByIndex(@NotNull TaskRemoveByIndexRequest request) {
        return call(request, TaskRemoveByIndexResponse.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public TaskShowByIdResponse taskShowById(@NotNull TaskShowByIdRequest request) {
        return call(request, TaskShowByIdResponse.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public TaskShowByIndexResponse taskShowByIndex(@NotNull TaskShowByIndexRequest request) {
        return call(request, TaskShowByIndexResponse.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public TaskShowByProjectIdResponse taskShowByProjectId(@NotNull TaskShowByProjectIdRequest request) {
        return call(request, TaskShowByProjectIdResponse.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public TaskStartByIdResponse taskStartById(@NotNull TaskStartByIdRequest request) {
        return call(request, TaskStartByIdResponse.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public TaskStartByIndexResponse taskStartByIndex(@NotNull TaskStartByIndexRequest request) {
        return call(request, TaskStartByIndexResponse.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public TaskUpdateByIdResponse taskUpdateById(@NotNull TaskUpdateByIdRequest request) {
        return call(request, TaskUpdateByIdResponse.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public TaskUpdateByIndexResponse taskUpdateByIndex(@NotNull TaskUpdateByIndexRequest request) {
        return call(request, TaskUpdateByIndexResponse.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public TaskBindToProjectResponse taskBindToProject(@NotNull TaskBindToProjectRequest request) {
        return call(request, TaskBindToProjectResponse.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public TaskUnbindFromProjectResponse taskUnbindFromProject(@NotNull TaskUnbindFromProjectRequest request) {
        return call(request, TaskUnbindFromProjectResponse.class);
    }

}
