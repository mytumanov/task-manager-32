package ru.mtumanov.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.api.endpoint.IUserEndpoint;
import ru.mtumanov.tm.dto.request.user.*;
import ru.mtumanov.tm.dto.response.user.*;

public class UserEndpointClient extends AbstractEndpoint implements IUserEndpoint {

    @Override
    @SneakyThrows
    @NotNull
    public UserChangePasswordResponse userChangePassword(@NotNull UserChangePasswordRequest request) {
        return call(request, UserChangePasswordResponse.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public UserLockResponse userLock(@NotNull UserLockRequest request) {
        return call(request, UserLockResponse.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public UserRegistryResponse userRegistry(@NotNull UserRegistryRequest request) {
        return call(request, UserRegistryResponse.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public UserRemoveResponse userRemove(@NotNull UserRemoveRequest request) {
        return call(request, UserRemoveResponse.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public UserUnlockResponse userUnlock(@NotNull UserUnlockRequest request) {
        return call(request, UserUnlockResponse.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public UserProfileResponse userProfile(@NotNull UserProfileRequest request) {
        return call(request, UserProfileResponse.class);
    }

    @Override
    @SneakyThrows
    @NotNull
    public UserUpdateResponse userUpdate(@NotNull UserUpdateRequest request) {
        return call(request, UserUpdateResponse.class);
    }

}
