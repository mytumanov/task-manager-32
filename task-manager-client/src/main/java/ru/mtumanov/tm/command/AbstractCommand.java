package ru.mtumanov.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.api.model.ICommand;
import ru.mtumanov.tm.api.service.IServiceLocator;

public abstract class AbstractCommand implements ICommand {

    @NotNull
    protected IServiceLocator serviceLocator;

    @NotNull
    protected IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @NotNull
    public String toString() {
        @Nullable final String name = getName();
        @Nullable final String argument = getArgument();
        @NotNull final String description = getDescription();
        String result = "";
        if (name != null && !name.isEmpty())
            result += name + " : ";
        if (argument != null && !argument.isEmpty())
            result += argument + " : ";
        if (!description.isEmpty())
            result += description;
        return result;
    }

}
