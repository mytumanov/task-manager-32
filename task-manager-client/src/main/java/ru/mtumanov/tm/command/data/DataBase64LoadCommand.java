package ru.mtumanov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.request.data.DataBase64LoadRequest;
import ru.mtumanov.tm.dto.response.data.DataBase64LoadResponse;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.AbstractException;

public class DataBase64LoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-base64";

    @Override
    @NotNull
    public String getDescription() {
        return "Load data from base64 file.";
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[DATA LOAD BASE64]");
        @NotNull final DataBase64LoadResponse response = getDomainEndpoint().loadDataBase64(new DataBase64LoadRequest());
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
