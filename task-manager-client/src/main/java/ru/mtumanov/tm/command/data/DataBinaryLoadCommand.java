package ru.mtumanov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.request.data.DataBinaryLoadRequest;
import ru.mtumanov.tm.dto.response.data.DataBinaryLoadResponse;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.AbstractException;

public class DataBinaryLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-bin";

    @Override
    @NotNull
    public String getDescription() {
        return "Load data from binary file.";
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[DATA BINARY LOAD]");
        @NotNull final DataBinaryLoadResponse response = getDomainEndpoint().loadDataBinary(new DataBinaryLoadRequest());
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
