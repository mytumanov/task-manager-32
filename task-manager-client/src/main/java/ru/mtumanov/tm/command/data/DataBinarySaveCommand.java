package ru.mtumanov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.request.data.DataBinarySaveRequest;
import ru.mtumanov.tm.dto.response.data.DataBinarySaveResponse;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.AbstractException;

public class DataBinarySaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-bin";

    @Override
    @NotNull
    public String getDescription() {
        return "Save data to binary file.";
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[SAVE DATA BINARY]");
        @NotNull final DataBinarySaveResponse response = getDomainEndpoint().saveDataBinary(new DataBinarySaveRequest());
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
