package ru.mtumanov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.request.data.DataJsonLoadJaxbRequest;
import ru.mtumanov.tm.dto.response.data.DataJsonLoadJaxbResponse;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.AbstractException;

public class DataJsonLoadJaxBCommand extends AbstractDataCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "Load data from json file";
    }

    @Override
    @NotNull
    public String getName() {
        return "data-load-json-jaxb";
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[DATA LOAD JSON]");
        @NotNull final DataJsonLoadJaxbResponse response = getDomainEndpoint().loadDataJsonJaxb(new DataJsonLoadJaxbRequest());
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
