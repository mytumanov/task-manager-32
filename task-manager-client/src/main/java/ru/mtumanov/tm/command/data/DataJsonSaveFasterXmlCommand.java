package ru.mtumanov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.request.data.DataJsonSaveFasterXmlRequest;
import ru.mtumanov.tm.dto.response.data.DataJsonSaveFasterXmlResponse;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.AbstractException;

public class DataJsonSaveFasterXmlCommand extends AbstractDataCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "Save data to json file";
    }

    @Override
    @NotNull
    public String getName() {
        return "data-save-json";
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[DATA SAVE JSON]");
        @NotNull final DataJsonSaveFasterXmlResponse response = getDomainEndpoint().saveDataJsonFasterXml(new DataJsonSaveFasterXmlRequest());
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
