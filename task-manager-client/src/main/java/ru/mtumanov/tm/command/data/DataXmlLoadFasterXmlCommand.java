package ru.mtumanov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.request.data.DataXmlLoadFasterXmlRequest;
import ru.mtumanov.tm.dto.response.data.DataXmlLoadFasterXmlResponse;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.AbstractException;

public class DataXmlLoadFasterXmlCommand extends AbstractDataCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "Load data from xml file";
    }

    @Override
    @NotNull
    public String getName() {
        return "data-load-xml";
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[DATA LOAD YAML]");
        @NotNull final DataXmlLoadFasterXmlResponse response = getDomainEndpoint().loadDataXmlFasterXml(new DataXmlLoadFasterXmlRequest());
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
