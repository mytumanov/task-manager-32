package ru.mtumanov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.request.data.DataXmlSaveFasterXmlRequest;
import ru.mtumanov.tm.dto.response.data.DataXmlSaveFasterXmlResponse;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.AbstractException;

public class DataXmlSaveFasterXmlCommand extends AbstractDataCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "Save data to xml file";
    }

    @Override
    @NotNull
    public String getName() {
        return "data-save-xml";
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[DATA SAVE XML]");
        @NotNull final DataXmlSaveFasterXmlResponse response = getDomainEndpoint().saveDataXmlFasterXml(new DataXmlSaveFasterXmlRequest());
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
