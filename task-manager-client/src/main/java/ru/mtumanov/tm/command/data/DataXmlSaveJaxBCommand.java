package ru.mtumanov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.request.data.DataXmlSaveJaxbRequest;
import ru.mtumanov.tm.dto.response.data.DataXmlSaveJaxbResponse;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.AbstractException;

public class DataXmlSaveJaxBCommand extends AbstractDataCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "Save data to xml file";
    }

    @Override
    @NotNull
    public String getName() {
        return "data-save-xml-jaxb";
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[DATA SAVE XML]");
        @NotNull final DataXmlSaveJaxbResponse response = getDomainEndpoint().saveDataXmlJaxb(new DataXmlSaveJaxbRequest());
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
