package ru.mtumanov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.request.data.DataYamlLoadFasterXmlRequest;
import ru.mtumanov.tm.dto.response.data.DataYamlLoadFasterXmlResponse;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.AbstractException;

public class DataYamlLoadFasterXmlCommand extends AbstractDataCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "Load data from yaml file";
    }

    @Override
    @NotNull
    public String getName() {
        return "data-load-yaml";
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[DATA LOAD YAML]");
        @NotNull final DataYamlLoadFasterXmlResponse response = getDomainEndpoint().loadDataYamlFasterXml(new DataYamlLoadFasterXmlRequest());
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
