package ru.mtumanov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.request.data.DataYamlSaveFasterXmlRequest;
import ru.mtumanov.tm.dto.response.data.DataYamlSaveFasterXmlResponse;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.AbstractException;

public class DataYamlSaveFasterXmlCommand extends AbstractDataCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "Save data to yaml file";
    }

    @Override
    @NotNull
    public String getName() {
        return "data-save-yaml";
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[DATA SAVE YAML]");
        @NotNull final DataYamlSaveFasterXmlResponse response = getDomainEndpoint().saveDataYamlFasterXml(new DataYamlSaveFasterXmlRequest());
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
