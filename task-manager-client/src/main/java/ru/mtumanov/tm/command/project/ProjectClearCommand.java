package ru.mtumanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.request.project.ProjectClearRequest;
import ru.mtumanov.tm.dto.response.project.ProjectClearResponse;
import ru.mtumanov.tm.exception.AbstractException;

public class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "Remove all projects";
    }

    @Override
    @NotNull
    public String getName() {
        return "project-clear";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[PROJECTS CLEAR]");
        @NotNull final ProjectClearRequest request = new ProjectClearRequest();
        @NotNull final ProjectClearResponse response = getProjectEndpoint().projectClear(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
