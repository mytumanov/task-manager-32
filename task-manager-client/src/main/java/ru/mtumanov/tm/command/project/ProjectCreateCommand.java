package ru.mtumanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.request.project.ProjectCreateRequest;
import ru.mtumanov.tm.dto.response.project.ProjectCreateResponse;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

public class ProjectCreateCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "Create new project";
    }

    @Override
    @NotNull
    public String getName() {
        return "project-create";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest(name, description);
        @NotNull final ProjectCreateResponse response = getProjectEndpoint().projectCreate(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
