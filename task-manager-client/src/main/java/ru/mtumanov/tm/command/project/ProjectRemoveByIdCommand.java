package ru.mtumanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.request.project.ProjectRemoveByIdRequest;
import ru.mtumanov.tm.dto.response.project.ProjectRemoveByIdResponse;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

public class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "Remove project by id";
    }

    @Override
    @NotNull
    public String getName() {
        return "project-remove-by-id";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(id);
        @NotNull final ProjectRemoveByIdResponse response = getProjectEndpoint().projectRemoveById(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
