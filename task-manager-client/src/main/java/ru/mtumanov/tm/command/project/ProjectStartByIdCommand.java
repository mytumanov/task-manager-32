package ru.mtumanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.request.project.ProjectStartByIdRequest;
import ru.mtumanov.tm.dto.response.project.ProjectStartByIdResponse;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

public class ProjectStartByIdCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "Start project by id";
    }

    @Override
    @NotNull
    public String getName() {
        return "project-start-by-id";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[START PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectStartByIdRequest request = new ProjectStartByIdRequest(id);
        @NotNull final ProjectStartByIdResponse response = getProjectEndpoint().projectStartById(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
