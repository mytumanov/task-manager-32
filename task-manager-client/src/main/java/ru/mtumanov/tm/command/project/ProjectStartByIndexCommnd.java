package ru.mtumanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.request.project.ProjectStartByIndexRequest;
import ru.mtumanov.tm.dto.response.project.ProjectStartByIndexResponse;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

public class ProjectStartByIndexCommnd extends AbstractProjectCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "Start project by index";
    }

    @Override
    @NotNull
    public String getName() {
        return "project-start-by-index";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final ProjectStartByIndexRequest request = new ProjectStartByIndexRequest(index);
        @NotNull final ProjectStartByIndexResponse response = getProjectEndpoint().projectStartByIndex(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
