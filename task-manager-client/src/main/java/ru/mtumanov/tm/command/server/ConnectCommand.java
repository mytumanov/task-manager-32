package ru.mtumanov.tm.command.server;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.command.AbstractCommand;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.server.TMConnectExceptio;

import java.io.IOException;
import java.net.Socket;

public class ConnectCommand extends AbstractCommand {

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public @NotNull String getDescription() {
        return "Connect to server";
    }

    @Override
    public @Nullable String getName() {
        return "connect";
    }

    @Override
    public @Nullable Role[] getRoles() {
        return null;
    }

    @Override
    public void execute() throws AbstractException {
        try {
            serviceLocator.getAuthEndpoint().connect();
            @Nullable final Socket socket = serviceLocator.getAuthEndpoint().getSocket();
            serviceLocator.getSystemEndpoint().setSocket(socket);
            serviceLocator.getDomainEndpoint().setSocket(socket);
            serviceLocator.getProjectEndpoint().setSocket(socket);
            serviceLocator.getTaskEndpoint().setSocket(socket);
            serviceLocator.getUserEndpoint().setSocket(socket);
        } catch (@NotNull final IOException e) {
            throw new TMConnectExceptio();
        }

    }

}
