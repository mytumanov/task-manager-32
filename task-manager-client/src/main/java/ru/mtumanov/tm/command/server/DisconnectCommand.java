package ru.mtumanov.tm.command.server;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.command.AbstractCommand;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.server.TMDisconnectException;

import java.io.IOException;

public class DisconnectCommand extends AbstractCommand {

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public @NotNull String getDescription() {
        return "Disconnect from server";
    }

    @Override
    public @Nullable String getName() {
        return "disconnect";
    }

    @Override
    public @Nullable Role[] getRoles() {
        return null;
    }

    @Override
    public void execute() throws AbstractException {
        try {
            serviceLocator.getAuthEndpoint().disconnect();
        } catch (@NotNull final IOException e) {
            throw new TMDisconnectException();
        }
    }

}
