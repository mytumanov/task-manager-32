package ru.mtumanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.request.task.TaskClearRequest;
import ru.mtumanov.tm.dto.response.task.TaskClearResponse;
import ru.mtumanov.tm.exception.AbstractException;

public class TaskClearCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "Remove all tasks";
    }

    @Override
    @NotNull
    public String getName() {
        return "task-clear";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[TASK CLEAR]");
        @NotNull final TaskClearRequest request = new TaskClearRequest();
        @NotNull final TaskClearResponse response = getTaskEndpoint().taskClear(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
