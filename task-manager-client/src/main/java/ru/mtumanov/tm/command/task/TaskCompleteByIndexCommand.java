package ru.mtumanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.request.task.TaskCompleteByIndexRequest;
import ru.mtumanov.tm.dto.response.task.TaskCompleteByIndexResponse;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

public class TaskCompleteByIndexCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "Complete task by index";
    }

    @Override
    @NotNull
    public String getName() {
        return "task-complete-by-index";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[COMPLETE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final TaskCompleteByIndexRequest request = new TaskCompleteByIndexRequest(index);
        @NotNull final TaskCompleteByIndexResponse response = getTaskEndpoint().taskCompleteByIndex(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
