package ru.mtumanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.request.task.TaskCreateRequest;
import ru.mtumanov.tm.dto.response.task.TaskCreateResponse;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

public class TaskCreateCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "Create new task";
    }

    @Override
    @NotNull
    public String getName() {
        return "task-create";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final TaskCreateRequest request = new TaskCreateRequest(name, description);
        @NotNull final TaskCreateResponse response = getTaskEndpoint().taskCreate(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
