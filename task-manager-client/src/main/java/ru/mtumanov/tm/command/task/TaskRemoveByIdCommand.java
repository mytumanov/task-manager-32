package ru.mtumanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.request.task.TaskRemoveByIdRequest;
import ru.mtumanov.tm.dto.response.task.TaskRemoveByIdResponse;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

public class TaskRemoveByIdCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "Remove task by id";
    }

    @Override
    @NotNull
    public String getName() {
        return "task-remove-by-id";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest(id);
        @NotNull final TaskRemoveByIdResponse response = getTaskEndpoint().taskRemoveById(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
