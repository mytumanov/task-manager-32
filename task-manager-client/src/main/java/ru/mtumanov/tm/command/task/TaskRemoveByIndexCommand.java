package ru.mtumanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.request.task.TaskRemoveByIndexRequest;
import ru.mtumanov.tm.dto.response.task.TaskRemoveByIndexResponse;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

public class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "Remove task by index";
    }

    @Override
    @NotNull
    public String getName() {
        return "task-remove-by-index";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final TaskRemoveByIndexRequest request = new TaskRemoveByIndexRequest(index);
        @NotNull final TaskRemoveByIndexResponse response = getTaskEndpoint().taskRemoveByIndex(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
