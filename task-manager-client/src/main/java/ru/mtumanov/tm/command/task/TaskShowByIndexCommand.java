package ru.mtumanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.request.task.TaskShowByIndexRequest;
import ru.mtumanov.tm.dto.response.task.TaskShowByIndexResponse;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

public class TaskShowByIndexCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "Show task by index";
    }

    @Override
    @NotNull
    public String getName() {
        return "task-show-by-index";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final TaskShowByIndexRequest request = new TaskShowByIndexRequest(index);
        @NotNull final TaskShowByIndexResponse response = getTaskEndpoint().taskShowByIndex(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
            return;
        }
        showTask(response.getTask());
    }

}
