package ru.mtumanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.request.task.TaskShowByProjectIdRequest;
import ru.mtumanov.tm.dto.response.task.TaskShowByProjectIdResponse;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

public class TaskShowByProjectIdCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "Show tasks by project id";
    }

    @Override
    @NotNull
    public String getName() {
        return "task-show-by-project-id";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @NotNull final TaskShowByProjectIdRequest request = new TaskShowByProjectIdRequest(projectId);
        @NotNull final TaskShowByProjectIdResponse response = getTaskEndpoint().taskShowByProjectId(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
            return;
        }
        renderTask(response.getTasks());
    }

}
