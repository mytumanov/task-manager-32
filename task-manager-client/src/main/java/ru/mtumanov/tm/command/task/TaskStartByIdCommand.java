package ru.mtumanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.request.task.TaskStartByIdRequest;
import ru.mtumanov.tm.dto.response.task.TaskStartByIdResponse;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

public class TaskStartByIdCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "Start task by id";
    }

    @Override
    @NotNull
    public String getName() {
        return "task-start-by-id";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[START TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskStartByIdRequest request = new TaskStartByIdRequest(id);
        @NotNull final TaskStartByIdResponse response = getTaskEndpoint().taskStartById(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
