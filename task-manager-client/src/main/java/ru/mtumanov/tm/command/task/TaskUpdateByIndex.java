package ru.mtumanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.request.task.TaskUpdateByIndexRequest;
import ru.mtumanov.tm.dto.response.task.TaskUpdateByIndexResponse;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

public class TaskUpdateByIndex extends AbstractTaskCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "Update task by index";
    }

    @Override
    @NotNull
    public String getName() {
        return "task-update-by-index";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final TaskUpdateByIndexRequest request = new TaskUpdateByIndexRequest(index, name, description);
        @NotNull final TaskUpdateByIndexResponse response = getTaskEndpoint().taskUpdateByIndex(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
