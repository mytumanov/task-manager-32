package ru.mtumanov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.request.user.UserLogoutRequest;
import ru.mtumanov.tm.dto.response.user.UserLogoutResponse;
import ru.mtumanov.tm.enumerated.Role;

public class UserLogoutCommand extends AbstractUserCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "logout current user";
    }

    @Override
    @NotNull
    public String getName() {
        return "logout";
    }

    @Override
    @Nullable
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        @NotNull final UserLogoutRequest request = new UserLogoutRequest();
        @NotNull final UserLogoutResponse response = getAuthEndpoint().logout(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
