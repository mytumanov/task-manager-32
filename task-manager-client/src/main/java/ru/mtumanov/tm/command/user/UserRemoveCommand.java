package ru.mtumanov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.request.user.UserRemoveRequest;
import ru.mtumanov.tm.dto.response.user.UserRemoveResponse;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

public class UserRemoveCommand extends AbstractUserCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "user remove";
    }

    @Override
    @NotNull
    public String getName() {
        return "user-remove";
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[USER REMOVE]");
        System.out.println("ENTER LOGIN");
        @NotNull final String login = TerminalUtil.nextLine();
        @NotNull final UserRemoveRequest request = new UserRemoveRequest(login);
        @NotNull final UserRemoveResponse response = getUserEndpoint().userRemove(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
