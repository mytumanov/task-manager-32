package ru.mtumanov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.request.user.UserUnlockRequest;
import ru.mtumanov.tm.dto.response.user.UserUnlockResponse;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

public class UserUnlockCommand extends AbstractUserCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "unlock user";
    }

    @Override
    @NotNull
    public String getName() {
        return "user-unlock";
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[USER UNLOCK]");
        System.out.println("ENTER LOGIN");
        @NotNull final String login = TerminalUtil.nextLine();
        @NotNull final UserUnlockRequest request = new UserUnlockRequest(login);
        @NotNull final UserUnlockResponse response = getUserEndpoint().userUnlock(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
