package ru.mtumanov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.request.user.UserProfileRequest;
import ru.mtumanov.tm.dto.response.user.UserProfileResponse;
import ru.mtumanov.tm.exception.AbstractException;

public class UserViewProfileCommand extends AbstractUserCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "view current user profile";
    }

    @Override
    @NotNull
    public String getName() {
        return "view-user-profile";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[USER VIEW PROFILE]");
        @NotNull final UserProfileRequest request = new UserProfileRequest();
        @NotNull final UserProfileResponse response = getUserEndpoint().userProfile(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
            return;
        }
        showUser(response.getUser());
    }

}
