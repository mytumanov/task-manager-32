package ru.mtumanov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.request.user.UserLoginRequest;
import ru.mtumanov.tm.dto.request.user.UserLogoutRequest;
import ru.mtumanov.tm.dto.response.user.UserLoginResponse;
import ru.mtumanov.tm.dto.response.user.UserLogoutResponse;

public interface IAuthEndpoint {

    @NotNull
    UserLoginResponse login(@NotNull UserLoginRequest request);

    @NotNull
    UserLogoutResponse logout(@NotNull UserLogoutRequest request);

}
