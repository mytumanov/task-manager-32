package ru.mtumanov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.request.server.ServerAboutRequest;
import ru.mtumanov.tm.dto.request.server.ServerVersionRequest;
import ru.mtumanov.tm.dto.response.server.ServerAboutResponse;
import ru.mtumanov.tm.dto.response.server.ServerVersionResponse;

public interface ISystemEndpoint {

    @NotNull
    ServerAboutResponse getAbout(@NotNull ServerAboutRequest serverAboutRequest);

    @NotNull
    ServerVersionResponse getVersion(@NotNull ServerVersionRequest serverVersionRequest);

}
