package ru.mtumanov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.request.user.*;
import ru.mtumanov.tm.dto.response.user.*;

public interface IUserEndpoint {

    @NotNull
    UserChangePasswordResponse userChangePassword(@NotNull UserChangePasswordRequest request);

    @NotNull
    UserLockResponse userLock(@NotNull UserLockRequest request);

    @NotNull
    UserRegistryResponse userRegistry(@NotNull UserRegistryRequest request);

    @NotNull
    UserRemoveResponse userRemove(@NotNull UserRemoveRequest request);

    @NotNull
    UserUnlockResponse userUnlock(@NotNull UserUnlockRequest request);

    @NotNull
    UserProfileResponse userProfile(@NotNull UserProfileRequest request);

    @NotNull
    UserUpdateResponse userUpdate(@NotNull UserUpdateRequest request);

}
