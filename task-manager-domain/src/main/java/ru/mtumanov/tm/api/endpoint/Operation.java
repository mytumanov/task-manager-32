package ru.mtumanov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.request.AbstractRequest;
import ru.mtumanov.tm.dto.response.AbstractResponse;

@FunctionalInterface
public interface Operation<RQ extends AbstractRequest, RS extends AbstractResponse> {

    @NotNull
    RS execute(@NotNull RQ request);

}
