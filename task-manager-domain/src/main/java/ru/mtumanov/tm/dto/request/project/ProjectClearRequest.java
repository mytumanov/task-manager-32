package ru.mtumanov.tm.dto.request.project;

import lombok.Getter;
import lombok.Setter;
import ru.mtumanov.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
public final class ProjectClearRequest extends AbstractUserRequest {

}