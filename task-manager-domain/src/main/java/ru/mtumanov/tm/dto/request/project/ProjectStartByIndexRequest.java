package ru.mtumanov.tm.dto.request.project;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
public class ProjectStartByIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer index;

    public ProjectStartByIndexRequest(@Nullable final Integer index) {
        this.index = index;
    }

}
