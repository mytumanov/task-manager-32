package ru.mtumanov.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.mtumanov.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class TaskClearRequest extends AbstractUserRequest {

}