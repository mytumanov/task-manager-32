package ru.mtumanov.tm.dto.request.task;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
public class TaskCompleteByIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer index;

    public TaskCompleteByIndexRequest(@Nullable final Integer index) {
        this.index = index;
    }

}
