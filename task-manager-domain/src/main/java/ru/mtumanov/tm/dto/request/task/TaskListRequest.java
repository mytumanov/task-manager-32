package ru.mtumanov.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.request.AbstractUserRequest;
import ru.mtumanov.tm.enumerated.TaskSort;

@Getter
@Setter
@NoArgsConstructor
public final class TaskListRequest extends AbstractUserRequest {

    @Nullable
    private TaskSort sort;

    public TaskListRequest(@Nullable final TaskSort sort) {
        this.sort = sort;
    }

}