package ru.mtumanov.tm.dto.request.task;

import lombok.Getter;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.request.AbstractUserRequest;

@Getter
public class TaskShowByProjectIdRequest extends AbstractUserRequest {

    @Nullable
    private String projectId;

    public TaskShowByProjectIdRequest(@Nullable final String projectId) {
        this.projectId = projectId;
    }

}
