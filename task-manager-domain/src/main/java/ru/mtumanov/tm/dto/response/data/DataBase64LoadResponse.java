package ru.mtumanov.tm.dto.response.data;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.response.AbstractResultResponse;

@NoArgsConstructor
public final class DataBase64LoadResponse extends AbstractResultResponse {

    public DataBase64LoadResponse(@NotNull final Throwable err) {
        super(err);
    }
}
