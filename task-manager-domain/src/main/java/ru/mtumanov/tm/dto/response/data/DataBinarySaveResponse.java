package ru.mtumanov.tm.dto.response.data;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.response.AbstractResultResponse;

@NoArgsConstructor
public final class DataBinarySaveResponse extends AbstractResultResponse {

    public DataBinarySaveResponse(@NotNull final Throwable err) {
        super(err);
    }

}
