package ru.mtumanov.tm.dto.response.project;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.response.AbstractResultResponse;
import ru.mtumanov.tm.model.Project;

public abstract class AbstractProjectResponse extends AbstractResultResponse {

    @Nullable
    @Getter
    private Project project;

    protected AbstractProjectResponse(@Nullable final Project project) {
        this.project = project;
    }

    protected AbstractProjectResponse(@NotNull Throwable err) {
        super(err);
    }

}
