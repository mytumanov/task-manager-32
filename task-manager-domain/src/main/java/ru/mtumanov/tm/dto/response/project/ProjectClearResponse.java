package ru.mtumanov.tm.dto.response.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.response.AbstractResultResponse;

@NoArgsConstructor
public final class ProjectClearResponse extends AbstractResultResponse {

    public ProjectClearResponse(@NotNull final Throwable err) {
        super(err);
    }
}