package ru.mtumanov.tm.dto.response.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.model.Project;

public class ProjectCompleteByIdResponse extends AbstractProjectResponse {

    public ProjectCompleteByIdResponse(@Nullable final Project project) {
        super(project);
    }

    public ProjectCompleteByIdResponse(@NotNull final Throwable err) {
        super(err);
    }

}
