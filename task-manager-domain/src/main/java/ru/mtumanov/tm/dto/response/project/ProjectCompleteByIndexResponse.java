package ru.mtumanov.tm.dto.response.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.model.Project;

public class ProjectCompleteByIndexResponse extends AbstractProjectResponse {

    public ProjectCompleteByIndexResponse(@Nullable final Project project) {
        super(project);
    }

    public ProjectCompleteByIndexResponse(@NotNull final Throwable err) {
        super(err);
    }

}
