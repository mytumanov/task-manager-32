package ru.mtumanov.tm.dto.response.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.response.AbstractResultResponse;
import ru.mtumanov.tm.model.Project;

import java.util.List;

@Getter
@NoArgsConstructor
public final class ProjectListResponse extends AbstractResultResponse {

    @Nullable
    private List<Project> projects;

    public ProjectListResponse(@Nullable final List<Project> projects) {
        this.projects = projects;
    }

    public ProjectListResponse(@NotNull final Throwable err) {
        super(err);
    }

}