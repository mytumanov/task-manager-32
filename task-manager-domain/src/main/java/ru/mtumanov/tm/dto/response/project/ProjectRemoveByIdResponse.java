package ru.mtumanov.tm.dto.response.project;

import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.model.Project;

public final class ProjectRemoveByIdResponse extends AbstractProjectResponse {

    public ProjectRemoveByIdResponse(@Nullable final Project project) {
        super(project);
    }

    public ProjectRemoveByIdResponse(@Nullable final Throwable err) {
        super(err);
    }

}