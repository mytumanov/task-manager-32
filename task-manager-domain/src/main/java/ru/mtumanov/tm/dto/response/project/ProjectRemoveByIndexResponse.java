package ru.mtumanov.tm.dto.response.project;

import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.model.Project;

public final class ProjectRemoveByIndexResponse extends AbstractProjectResponse {

    public ProjectRemoveByIndexResponse(@Nullable final Project project) {
        super(project);
    }

    public ProjectRemoveByIndexResponse(@Nullable final Throwable err) {
        super(err);
    }

}