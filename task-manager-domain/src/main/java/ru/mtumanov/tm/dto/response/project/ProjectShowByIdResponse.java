package ru.mtumanov.tm.dto.response.project;

import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.model.Project;

public final class ProjectShowByIdResponse extends AbstractProjectResponse {

    public ProjectShowByIdResponse(@Nullable final Project project) {
        super(project);
    }

    public ProjectShowByIdResponse(@Nullable final Throwable err) {
        super(err);
    }

}