package ru.mtumanov.tm.dto.response.project;

import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.model.Project;

public class ProjectStartByIdResponse extends AbstractProjectResponse {

    public ProjectStartByIdResponse(@Nullable final Project project) {
        super(project);
    }

    public ProjectStartByIdResponse(@Nullable final Throwable err) {
        super(err);
    }

}
