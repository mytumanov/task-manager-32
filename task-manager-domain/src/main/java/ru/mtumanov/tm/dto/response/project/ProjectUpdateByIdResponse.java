package ru.mtumanov.tm.dto.response.project;

import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.model.Project;

public final class ProjectUpdateByIdResponse extends AbstractProjectResponse {

    public ProjectUpdateByIdResponse(@Nullable final Project project) {
        super(project);
    }

    public ProjectUpdateByIdResponse(@Nullable final Throwable err) {
        super(err);
    }

}