package ru.mtumanov.tm.dto.response.project;

import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.model.Project;

public final class ProjectUpdateByIndexResponse extends AbstractProjectResponse {

    public ProjectUpdateByIndexResponse(@Nullable final Project project) {
        super(project);
    }

    public ProjectUpdateByIndexResponse(@Nullable final Throwable err) {
        super(err);
    }

}