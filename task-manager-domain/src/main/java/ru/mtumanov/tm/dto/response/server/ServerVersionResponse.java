package ru.mtumanov.tm.dto.response.server;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.response.AbstractResponse;

@Getter
@Setter
public class ServerVersionResponse extends AbstractResponse {

    @Nullable
    private String version;

}
