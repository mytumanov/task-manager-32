package ru.mtumanov.tm.dto.response.task;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.response.AbstractResultResponse;
import ru.mtumanov.tm.model.Task;

@Getter
public abstract class AbstractTaskResponse extends AbstractResultResponse {

    @Nullable
    private Task task;

    protected AbstractTaskResponse(@Nullable final Task task) {
        this.task = task;
    }

    protected AbstractTaskResponse(@NotNull final Throwable err) {
        super(err);
    }

}