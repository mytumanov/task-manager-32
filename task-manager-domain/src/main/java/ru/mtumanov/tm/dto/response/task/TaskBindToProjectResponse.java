package ru.mtumanov.tm.dto.response.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.response.AbstractResultResponse;

@NoArgsConstructor
public final class TaskBindToProjectResponse extends AbstractResultResponse {

    public TaskBindToProjectResponse(@NotNull final Throwable err) {
        super(err);
    }

}