package ru.mtumanov.tm.dto.response.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.model.Task;

public class TaskCompleteByIdResponse extends AbstractTaskResponse {

    public TaskCompleteByIdResponse(@Nullable final Task task) {
        super(task);
    }

    public TaskCompleteByIdResponse(@NotNull final Throwable err) {
        super(err);
    }

}
