package ru.mtumanov.tm.dto.response.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.model.Task;

public class TaskCompleteByIndexResponse extends AbstractTaskResponse {

    public TaskCompleteByIndexResponse(@Nullable final Task task) {
        super(task);
    }

    public TaskCompleteByIndexResponse(@NotNull final Throwable err) {
        super(err);
    }

}
