package ru.mtumanov.tm.dto.response.task;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.response.AbstractResultResponse;
import ru.mtumanov.tm.model.Task;

import java.util.List;

public class TaskShowByProjectIdResponse extends AbstractResultResponse {

    @Nullable
    @Getter
    private List<Task> tasks;

    public TaskShowByProjectIdResponse(@Nullable final List<Task> tasks) {
        this.tasks = tasks;
    }

    public TaskShowByProjectIdResponse(@NotNull final Throwable err) {
        super(err);
    }

}
