package ru.mtumanov.tm.dto.response.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.model.Task;

public class TaskStartByIdResponse extends AbstractTaskResponse {

    public TaskStartByIdResponse(@Nullable final Task task) {
        super(task);
    }

    public TaskStartByIdResponse(@NotNull final Throwable err) {
        super(err);
    }

}
