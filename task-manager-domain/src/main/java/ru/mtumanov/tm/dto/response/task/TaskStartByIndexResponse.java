package ru.mtumanov.tm.dto.response.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.model.Task;

public class TaskStartByIndexResponse extends AbstractTaskResponse {

    public TaskStartByIndexResponse(@Nullable final Task task) {
        super(task);
    }

    public TaskStartByIndexResponse(@NotNull final Throwable err) {
        super(err);
    }

}
