package ru.mtumanov.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.response.AbstractResultResponse;
import ru.mtumanov.tm.model.User;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractUserResponse extends AbstractResultResponse {

    @Nullable
    private User user;

    protected AbstractUserResponse(@Nullable final User user) {
        this.user = user;
    }

    protected AbstractUserResponse(@NotNull final Throwable err) {
        super(err);
    }

}