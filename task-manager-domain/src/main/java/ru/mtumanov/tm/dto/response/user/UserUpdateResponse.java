package ru.mtumanov.tm.dto.response.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.model.User;

@NoArgsConstructor
public final class UserUpdateResponse extends AbstractUserResponse {

    public UserUpdateResponse(@Nullable final User user) {
        super(user);
    }

    public UserUpdateResponse(@NotNull final Throwable err) {
        super(err);
    }

}