package ru.mtumanov.tm.endpoint;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.api.service.IServiceLocator;
import ru.mtumanov.tm.api.service.IUserService;
import ru.mtumanov.tm.dto.request.AbstractUserRequest;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.user.AccessDeniedException;
import ru.mtumanov.tm.exception.user.PermissionException;
import ru.mtumanov.tm.model.User;

public abstract class AbstractEndpoint {

    @NotNull
    @Getter
    private final IServiceLocator serviceLocator;

    protected AbstractEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    protected void check(@Nullable final AbstractUserRequest request, @Nullable final Role role) throws AbstractException {
        if (request == null || role == null) throw new AccessDeniedException();
        @Nullable final String userId = request.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @NotNull final IUserService userService = serviceLocator.getUserService();
        @Nullable final User user = userService.findOneById(userId);
        @Nullable final Role userRole = user.getRole();
        final boolean check = userRole == role;
        if (!check) throw new PermissionException();
    }

    protected void check(@Nullable final AbstractUserRequest request) throws AbstractException {
        if (request == null) throw new AccessDeniedException();
        @Nullable final String userId = request.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
    }

}
