package ru.mtumanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;

import ru.mtumanov.tm.api.endpoint.IAuthEndpoint;
import ru.mtumanov.tm.dto.request.user.UserLoginRequest;
import ru.mtumanov.tm.dto.request.user.UserLogoutRequest;
import ru.mtumanov.tm.dto.response.user.UserLoginResponse;
import ru.mtumanov.tm.dto.response.user.UserLogoutResponse;

public class AuthEndpoint implements IAuthEndpoint {

    @Override
    @NotNull
    public UserLoginResponse login(@NotNull UserLoginRequest request) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    @NotNull
    public UserLogoutResponse logout(@NotNull UserLogoutRequest request) {
        // TODO Auto-generated method stub
        return null;
    }
    
}
