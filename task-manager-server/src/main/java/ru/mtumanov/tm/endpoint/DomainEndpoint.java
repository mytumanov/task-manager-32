package ru.mtumanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.api.endpoint.IDomainEndpoint;
import ru.mtumanov.tm.api.service.IServiceLocator;
import ru.mtumanov.tm.dto.request.data.*;
import ru.mtumanov.tm.dto.response.data.*;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.AbstractException;

public class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    public DomainEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public DataBase64LoadResponse loadDataBase64(@NotNull DataBase64LoadRequest request) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().loadDataBase64();
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataBase64LoadResponse(e);
        }
        return new DataBase64LoadResponse();
    }

    @Override
    @NotNull
    public DataBase64SaveResponse saveDataBase64(@NotNull DataBase64SaveRequest request) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().saveDataBase64();
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataBase64SaveResponse(e);
        }
        return new DataBase64SaveResponse();
    }

    @Override
    @NotNull
    public DataBinaryLoadResponse loadDataBinary(@NotNull DataBinaryLoadRequest request) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().loadDataBinary();
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataBinaryLoadResponse(e);
        }
        return new DataBinaryLoadResponse();
    }

    @Override
    @NotNull
    public DataBinarySaveResponse saveDataBinary(@NotNull DataBinarySaveRequest request) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().saveDataBinary();
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataBinarySaveResponse(e);
        }
        return new DataBinarySaveResponse();
    }

    @Override
    @NotNull
    public DataJsonLoadFasterXmlResponse loadDataJsonFasterXml(@NotNull DataJsonLoadFasterXmlRequest request) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().loadDataJsonFasterXml();
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataJsonLoadFasterXmlResponse(e);
        }
        return new DataJsonLoadFasterXmlResponse();
    }

    @Override
    @NotNull
    public DataJsonSaveFasterXmlResponse saveDataJsonFasterXml(@NotNull DataJsonSaveFasterXmlRequest request) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().saveDataJsonFasterXml();
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataJsonSaveFasterXmlResponse(e);
        }
        return new DataJsonSaveFasterXmlResponse();
    }

    @Override
    @NotNull
    public DataJsonLoadJaxbResponse loadDataJsonJaxb(@NotNull DataJsonLoadJaxbRequest request) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().loadDataJsonJaxb();
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataJsonLoadJaxbResponse(e);
        }
        return new DataJsonLoadJaxbResponse();
    }

    @Override
    @NotNull
    public DataJsonSaveJaxbResponse saveDataJsonJaxb(@NotNull DataJsonSaveJaxbRequest request) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().saveDataJsonJaxb();
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataJsonSaveJaxbResponse(e);
        }
        return new DataJsonSaveJaxbResponse();
    }

    @Override
    @NotNull
    public DataXmlLoadFasterXmlResponse loadDataXmlFasterXml(@NotNull DataXmlLoadFasterXmlRequest request) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().loadDataXmlFasterXml();
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataXmlLoadFasterXmlResponse(e);
        }
        return new DataXmlLoadFasterXmlResponse();
    }

    @Override
    @NotNull
    public DataXmlSaveFasterXmlResponse saveDataXmlFasterXml(@NotNull DataXmlSaveFasterXmlRequest request) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().saveDataXmlFasterXml();
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataXmlSaveFasterXmlResponse(e);
        }
        return new DataXmlSaveFasterXmlResponse();
    }

    @Override
    @NotNull
    public DataXmlLoadJaxbResponse loadDataXmlJaxb(@NotNull DataXmlLoadJaxbRequest request) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().loadDataXmlJaxb();
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataXmlLoadJaxbResponse(e);
        }
        return new DataXmlLoadJaxbResponse();
    }

    @Override
    @NotNull
    public DataXmlSaveJaxbResponse saveDataXmlJaxb(@NotNull DataXmlSaveJaxbRequest request) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().saveDataXmlJaxb();
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataXmlSaveJaxbResponse(e);
        }
        return new DataXmlSaveJaxbResponse();
    }

    @Override
    @NotNull
    public DataYamlLoadFasterXmlResponse loadDataYamlFasterXml(@NotNull DataYamlLoadFasterXmlRequest request) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().loadDataYamlFasterXml();
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataYamlLoadFasterXmlResponse(e);
        }
        return new DataYamlLoadFasterXmlResponse();
    }

    @Override
    @NotNull
    public DataYamlSaveFasterXmlResponse saveDataYamlFasterXml(@NotNull DataYamlSaveFasterXmlRequest request) {
        try {
            check(request, Role.ADMIN);
            getServiceLocator().getDomainService().saveDataYamlFasterXml();
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new DataYamlSaveFasterXmlResponse(e);
        }
        return new DataYamlSaveFasterXmlResponse();
    }

}
