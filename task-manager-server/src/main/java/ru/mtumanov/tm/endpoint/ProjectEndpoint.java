package ru.mtumanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.api.endpoint.IProjectEndpoint;
import ru.mtumanov.tm.api.service.IServiceLocator;
import ru.mtumanov.tm.dto.request.project.*;
import ru.mtumanov.tm.dto.response.project.*;
import ru.mtumanov.tm.enumerated.ProjectSort;
import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.model.Project;

import javax.annotation.Nullable;

import java.util.Comparator;
import java.util.List;

public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public ProjectChangeStatusByIdResponse projectChangeStatusById(
            @NotNull final ProjectChangeStatusByIdRequest request
    ) {
        try {
            check(request);
            @Nullable final String projectId = request.getId();
            @Nullable final Status status = request.getStatus();
            @Nullable final String userId = request.getUserId();
            @NotNull final Project project = getServiceLocator().getProjectService().changeProjectStatusById(userId, projectId, status);
            return new ProjectChangeStatusByIdResponse(project);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new ProjectChangeStatusByIdResponse(e);
        }
    }

    @Override
    @NotNull
    public ProjectChangeStatusByIndexResponse projectChangeStatusByIndex(
            @NotNull final ProjectChangeStatusByIndexRequest request
    ) {
        try {
            check(request);
            @Nullable final Integer index = request.getIndex();
            @Nullable final Status status = request.getStatus();
            @Nullable final String userId = request.getUserId();
            @NotNull final Project project = getServiceLocator().getProjectService().changeProjectStatusByIndex(userId, index, status);
            return new ProjectChangeStatusByIndexResponse(project);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new ProjectChangeStatusByIndexResponse(e);
        }
    }

    @Override
    @NotNull
    public ProjectClearResponse projectClear(@NotNull final ProjectClearRequest request) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            getServiceLocator().getProjectService().clear(userId);
            return new ProjectClearResponse();
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new ProjectClearResponse(e);
        }
    }

    @Override
    @NotNull
    public ProjectCompleteByIdResponse projectCompleteById(@NotNull final ProjectCompleteByIdRequest request) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final String id = request.getId();
            @NotNull Project project = getServiceLocator().getProjectService().changeProjectStatusById(userId, id, Status.COMPLETED);
            return new ProjectCompleteByIdResponse(project);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new ProjectCompleteByIdResponse(e);
        }
    }

    @Override
    @NotNull
    public ProjectCompleteByIndexResponse projectCompleteByIndex(
            @NotNull final ProjectCompleteByIndexRequest request
    ) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final Integer index = request.getIndex();
            @NotNull Project project = getServiceLocator().getProjectService().changeProjectStatusByIndex(userId, index, Status.COMPLETED);
            return new ProjectCompleteByIndexResponse(project);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new ProjectCompleteByIndexResponse(e);
        }
    }

    @Override
    @NotNull
    public ProjectCreateResponse projectCreate(@NotNull final ProjectCreateRequest request) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final String name = request.getName();
            @Nullable final String description = request.getDescription();
            @NotNull Project project = getServiceLocator().getProjectService().create(userId, name, description);
            return new ProjectCreateResponse(project);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new ProjectCreateResponse(e);
        }
    }

    @Override
    @NotNull
    public ProjectListResponse projectList(@NotNull final ProjectListRequest request) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final ProjectSort sort = request.getSort();
            Comparator<Project> comparator = null;
            if (sort != null)
                comparator = sort.getComparator();
            @NotNull List<Project> projects = getServiceLocator().getProjectService().findAll(userId, comparator);
            return new ProjectListResponse(projects);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new ProjectListResponse(e);
        }
    }

    @Override
    @NotNull
    public ProjectRemoveByIdResponse projectRemoveById(@NotNull final ProjectRemoveByIdRequest request) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final String id = request.getId();
            @NotNull Project project = getServiceLocator().getProjectService().removeById(userId, id);
            return new ProjectRemoveByIdResponse(project);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new ProjectRemoveByIdResponse(e);
        }
    }

    @Override
    @NotNull
    public ProjectRemoveByIndexResponse projectRemoveByIndex(@NotNull final ProjectRemoveByIndexRequest request) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final Integer index = request.getIndex();
            @NotNull Project project = getServiceLocator().getProjectService().removeByIndex(userId, index);
            return new ProjectRemoveByIndexResponse(project);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new ProjectRemoveByIndexResponse(e);
        }
    }

    @Override
    @NotNull
    public ProjectShowByIdResponse projectShowById(@NotNull final ProjectShowByIdRequest request) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final String id = request.getId();
            @NotNull Project project = getServiceLocator().getProjectService().findOneById(userId, id);
            return new ProjectShowByIdResponse(project);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new ProjectShowByIdResponse(e);
        }
    }

    @Override
    @NotNull
    public ProjectShowByIndexResponse projectShowByIndex(@NotNull final ProjectShowByIndexRequest request) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final Integer index = request.getIndex();
            @NotNull Project project = getServiceLocator().getProjectService().findOneByIndex(userId, index);
            return new ProjectShowByIndexResponse(project);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new ProjectShowByIndexResponse(e);
        }
    }

    @Override
    @NotNull
    public ProjectStartByIdResponse projectStartById(@NotNull final ProjectStartByIdRequest request) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final String id = request.getId();
            @NotNull Project project = getServiceLocator().getProjectService().changeProjectStatusById(userId, id, Status.IN_PROGRESS);
            return new ProjectStartByIdResponse(project);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new ProjectStartByIdResponse(e);
        }
    }

    @Override
    @NotNull
    public ProjectStartByIndexResponse projectStartByIndex(@NotNull final ProjectStartByIndexRequest request) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final Integer index = request.getIndex();
            @NotNull Project project = getServiceLocator().getProjectService().changeProjectStatusByIndex(userId, index, Status.IN_PROGRESS);
            return new ProjectStartByIndexResponse(project);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new ProjectStartByIndexResponse(e);
        }
    }

    @Override
    @NotNull
    public ProjectUpdateByIdResponse projectUpdateById(@NotNull final ProjectUpdateByIdRequest request) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final String id = request.getId();
            @Nullable final String name = request.getName();
            @Nullable final String description = request.getDescription();
            @NotNull Project project = getServiceLocator().getProjectService().updateById(userId, id, name, description);
            return new ProjectUpdateByIdResponse(project);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new ProjectUpdateByIdResponse(e);
        }
    }

    @Override
    @NotNull
    public ProjectUpdateByIndexResponse projectUpdateByIndex(@NotNull final ProjectUpdateByIndexRequest request) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @Nullable final Integer index = request.getIndex();
            @Nullable final String name = request.getName();
            @Nullable final String description = request.getDescription();
            @NotNull Project project = getServiceLocator().getProjectService().updateByIndex(userId, index, name, description);
            return new ProjectUpdateByIndexResponse(project);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new ProjectUpdateByIndexResponse(e);
        }
    }

}
