package ru.mtumanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.api.endpoint.IUserEndpoint;
import ru.mtumanov.tm.api.service.IServiceLocator;
import ru.mtumanov.tm.dto.request.user.*;
import ru.mtumanov.tm.dto.response.user.*;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.model.User;

public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public UserChangePasswordResponse userChangePassword(@NotNull final UserChangePasswordRequest request) {
        try {
            check(request);
            @Nullable final String id = request.getUserId();
            @Nullable final String password = request.getPassword();
            @NotNull final User user = getServiceLocator().getUserService().setPassword(id, password);
            return new UserChangePasswordResponse(user);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new UserChangePasswordResponse(e);
        }
    }

    @Override
    @NotNull
    public UserLockResponse userLock(@NotNull final UserLockRequest request) {
        try {
            check(request, Role.ADMIN);
            @Nullable final String login = request.getLogin();
            getServiceLocator().getUserService().lockUserByLogin(login);
            return new UserLockResponse();
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new UserLockResponse(e);
        }
    }

    @Override
    @NotNull
    public UserRegistryResponse userRegistry(@NotNull final UserRegistryRequest request) {
        try {
            @Nullable final String login = request.getLogin();
            @Nullable final String password = request.getPassword();
            @Nullable final String email = request.getEmail();
            @NotNull final User user = getServiceLocator().getUserService().create(login, password, email);
            return new UserRegistryResponse(user);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new UserRegistryResponse(e);
        }
    }

    @Override
    @NotNull
    public UserRemoveResponse userRemove(@NotNull final UserRemoveRequest request) {
        try {
            check(request, Role.ADMIN);
            @Nullable final String login = request.getLogin();
            @NotNull final User user = getServiceLocator().getUserService().removeByLogin(login);
            return new UserRemoveResponse(user);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new UserRemoveResponse(e);
        }
    }

    @Override
    @NotNull
    public UserUnlockResponse userUnlock(@NotNull final UserUnlockRequest request) {
        try {
            check(request, Role.ADMIN);
            @Nullable final String login = request.getLogin();
            getServiceLocator().getUserService().unlockUserByLogin(login);
            return new UserUnlockResponse();
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new UserUnlockResponse(e);
        }
    }

    @Override
    @NotNull
    public UserProfileResponse userProfile(@NotNull final UserProfileRequest request) {
        try {
            check(request);
            @Nullable final String userId = request.getUserId();
            @NotNull final User user = getServiceLocator().getUserService().findOneById(userId);
            return new UserProfileResponse(user);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new UserProfileResponse(e);
        }
    }

    @Override
    @NotNull
    public UserUpdateResponse userUpdate(@NotNull UserUpdateRequest request) {
        try {
            check(request);
            @NotNull final String id = request.getUserId();
            @NotNull final String firstName = request.getFirstName();
            @NotNull final String lastName = request.getLastName();
            @NotNull final String middleName = request.getMiddleName();
            @NotNull final User user = getServiceLocator().getUserService().userUpdate(id, firstName, lastName, middleName);
            return new UserUpdateResponse(user);
        } catch (@NotNull final AbstractException e) {
            getServiceLocator().getLoggerService().error(e);
            return new UserUpdateResponse(e);
        }
    }

}
