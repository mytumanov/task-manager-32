package ru.mtumanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.api.repository.IRepository;
import ru.mtumanov.tm.exception.entity.AbstractEntityNotFoundException;
import ru.mtumanov.tm.exception.entity.EntityNotFoundException;
import ru.mtumanov.tm.model.AbstractModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected final List<M> models = new ArrayList<>();

    @Override
    @NotNull
    public M add(@NotNull final M model) {
        models.add(model);
        return model;
    }

    @Override
    @NotNull
    public Collection<M> add(@NotNull final Collection<M> models) {
        this.models.addAll(models);
        return models;
    }

    @Override
    @NotNull
    public Collection<M> set(@NotNull final Collection<M> models) {
        clear();
        return add(models);
    }

    @Override
    @NotNull
    public List<M> findAll() {
        return models;
    }

    @Override
    @NotNull
    public List<M> findAll(@NotNull final Comparator<M> comparator) {
        return models.stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    @NotNull
    public M findOneById(@NotNull final String id) throws AbstractEntityNotFoundException {
        return models.stream()
                .filter(m -> id.equals(m.getId()))
                .findFirst()
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @NotNull
    public M findOneByIndex(@NotNull final Integer index) {
        return models.get(index);
    }

    @Override
    @NotNull
    public M remove(@NotNull final M model) {
        models.remove(model);
        return model;
    }

    @Override
    @NotNull
    public M removeById(@NotNull final String id) throws AbstractEntityNotFoundException {
        @NotNull final M model = findOneById(id);
        models.remove(model);
        return model;
    }

    @Override
    @NotNull
    public M removeByIndex(@NotNull final Integer index) throws AbstractEntityNotFoundException {
        @Nullable final M model = models.get(index);
        if (model == null)
            throw new EntityNotFoundException();
        models.remove(model);
        return model;
    }

    @Override
    public void clear() {
        models.clear();
    }

    @Override
    public int getSize() {
        return models.size();
    }

    @Override
    public boolean existById(@NotNull final String id) {
        try {
            findOneById(id);
            return true;
        } catch (AbstractEntityNotFoundException e) {
            return false;
        }
    }

}
