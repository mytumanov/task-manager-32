package ru.mtumanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.api.service.IAuthService;
import ru.mtumanov.tm.api.service.IPropertyService;
import ru.mtumanov.tm.api.service.IUserService;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.field.LoginEmptyException;
import ru.mtumanov.tm.exception.field.PasswordEmptyException;
import ru.mtumanov.tm.exception.user.AccessDeniedException;
import ru.mtumanov.tm.exception.user.AuthenticationException;
import ru.mtumanov.tm.exception.user.UserNotFoundException;
import ru.mtumanov.tm.model.User;
import ru.mtumanov.tm.util.HashUtil;

public class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    @Nullable
    private String userId;

    public AuthService(@NotNull IUserService userService, @NotNull final IPropertyService propertyService) {
        this.userService = userService;
        this.propertyService = propertyService;
    }

    @Override
    @NotNull
    public User check(@Nullable String login, @Nullable String password) throws AbstractException {
        if (login == null || login.isEmpty())
            throw new LoginEmptyException();
        if (password == null || password.isEmpty())
            throw new PasswordEmptyException();
        @NotNull final User user;
        try {
            user = userService.findByLogin(login);
        } catch (UserNotFoundException e) {
            throw new AccessDeniedException();
        }
        if (user.getPasswordHash() == null)
            throw new AccessDeniedException();
        if (user.isLocked())
            throw new AuthenticationException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (!user.getPasswordHash().equals(hash))
            throw new AccessDeniedException();
        userId = user.getId();
        return user;
    }

}
