package ru.mtumanov.tm.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.mtumanov.tm.component.Server;

import java.net.Socket;

public abstract class AbstractServerSocketTask extends AbstractServerTask {

    @NotNull
    protected final Socket socket;

    @Nullable
    protected String userId;

    protected AbstractServerSocketTask(@NotNull final Server server, @NotNull final Socket socket) {
        super(server);
        this.socket = socket;
    }

    protected AbstractServerSocketTask(@NotNull final Server server, @NotNull final Socket socket, @NotNull String userId) {
        super(server);
        this.socket = socket;
        this.userId = userId;
    }

}
