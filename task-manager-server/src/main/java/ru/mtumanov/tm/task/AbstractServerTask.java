package ru.mtumanov.tm.task;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.component.Server;

public abstract class AbstractServerTask implements Runnable {

    @NotNull
    protected Server server;

    protected AbstractServerTask(@NotNull final Server server) {
        this.server = server;
    }

}
