package ru.mtumanov.tm.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.component.Server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerAcceptTask extends AbstractServerTask {

    public ServerAcceptTask(@NotNull final Server server) {
        super(server);
    }

    @Override
    public void run() {
        @Nullable final ServerSocket serverSocket = server.getServerSocket();
        if (serverSocket == null) return;
        try {
            @NotNull final Socket socket = serverSocket.accept();
            server.submit(new ServerRequestTask(server, socket));
            server.submit(new ServerAcceptTask(server));
        } catch (@NotNull final IOException e) {
            server.getLoggerService().error(e);
        }
    }

}