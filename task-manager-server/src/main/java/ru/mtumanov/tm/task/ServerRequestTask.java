package ru.mtumanov.tm.task;

import org.jetbrains.annotations.NotNull;

import ru.mtumanov.tm.api.service.IAuthService;
import ru.mtumanov.tm.component.Server;
import ru.mtumanov.tm.dto.request.AbstractRequest;
import ru.mtumanov.tm.dto.request.AbstractUserRequest;
import ru.mtumanov.tm.dto.request.user.UserLoginRequest;
import ru.mtumanov.tm.dto.request.user.UserLogoutRequest;
import ru.mtumanov.tm.dto.response.AbstractResponse;
import ru.mtumanov.tm.dto.response.ApplicationErrorResponse;
import ru.mtumanov.tm.dto.response.user.UserLoginResponse;
import ru.mtumanov.tm.dto.response.user.UserLogoutResponse;
import ru.mtumanov.tm.model.User;

import javax.annotation.Nullable;
import java.io.*;
import java.net.Socket;

public class ServerRequestTask extends AbstractServerSocketTask {

    @Nullable
    private AbstractRequest request;

    @Nullable
    private AbstractResponse response;

    public ServerRequestTask(@NotNull final Server server, @NotNull final Socket socket) {
        super(server, socket);
    }

    public ServerRequestTask(@NotNull final Server server, @NotNull final Socket socket, @NotNull final String userId) {
        super(server, socket, userId);
    }

    @Override
    public void run() {
        try {
            processInput();
            processUserId();
            processLogin();
            // processProfile();
            processLogout();
            processOperation();
            processOutput();
        } catch (@NotNull final EOFException e) {
            server.getLoggerService().debug(e.getMessage());
        } catch (@NotNull final IOException | ClassNotFoundException e) {
            server.getLoggerService().error(e);
        }
    }

    private void processInput() throws IOException, ClassNotFoundException{
        @NotNull final InputStream inputStream = socket.getInputStream();
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
        @NotNull final Object object = objectInputStream.readObject();
        request = (AbstractRequest) object;
    }

    private void processOutput() throws IOException {
        @NotNull final OutputStream outputStream = socket.getOutputStream();
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
        objectOutputStream.writeObject(response);
        server.submit(new ServerRequestTask(server, socket, userId));
    }

    private void processUserId() {
        if (!(request instanceof AbstractUserRequest)) return;
        @NotNull final AbstractUserRequest abstractUserRequest = (AbstractUserRequest) request;
        abstractUserRequest.setUserId(userId);
    }

    private void processLogin() {
        if (response != null) return;
        if (!(request instanceof UserLoginRequest)) return;
        try {
            @NotNull final UserLoginRequest userLoginRequest = (UserLoginRequest) request;
            @Nullable final String login = userLoginRequest.getLogin();
            @Nullable final String password = userLoginRequest.getPassword();
            @NotNull final IAuthService authService = server.getBootstrap().getAuthService();
            @NotNull final User user = authService.check(login, password);
            userId = user.getId();
            response = new UserLoginResponse();
        } catch (@NotNull final Exception e) {
            response = new UserLoginResponse(e);
        }
    }

    // private void processProfile() {
    //     if (response != null) return;
    //     if (!(request instanceof UserProfileRequest)) return;
    //     if (userId == null) {
    //         response = new UserProfileResponse();
    //         return;
    //     }
    //     @NotNull final IUserService userService = server.getBootstrap().getUserService();
    //     @Nullable final User user = userService.findOneById(userId);
    //     response = new UserProfileResponse(user);
    // }

    private void processLogout() {
        if (response != null) return;
        if (!(request instanceof UserLogoutRequest)) return;
        userId = null;
        response = new UserLogoutResponse();
    }

    private void processOperation() {
        if (response != null) return;
        try {
            if (request == null) return;
            @Nullable final Object result = server.call(request);
            response = (AbstractResponse) result;
        } catch (@NotNull Exception e) {
            e.printStackTrace();
            response = new ApplicationErrorResponse(e);
        }
    }

}
